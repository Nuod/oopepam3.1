public class DemoLib {
    public void Start() {
        Library lib1 = new Library();
        ServiceLibrary ser1 = new ServiceLibrary();
        System.out.println(lib1);
        lib1.setStorage(ser1.AddBook(lib1, new Book("1","Who?")));
        lib1.setStorage(ser1.AddBook(lib1, new Book("2","What?")));
        lib1.setStorage(ser1.AddBook(lib1, new Book("3","Where?")));
        lib1.setStorage(ser1.AddBook(lib1, new Book("4","How?")));
        System.out.println(lib1);
        lib1.setStorage(ser1.DelBook(lib1, new Book("3","Where?")));
        lib1.setStorage(ser1.DelBook(lib1, new Book("4","How?")));
        System.out.println(lib1);
        System.out.println(ser1.FindBookByName(lib1, new Book("2","What?")));
        System.out.println(ser1.FindBookByName(lib1, new Book("4","How?")));
    }

}
