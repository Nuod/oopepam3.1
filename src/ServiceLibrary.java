import java.util.Arrays;
import java.util.Comparator;

public class ServiceLibrary {
    public Book[] AddBook(Library library, Book book) {

       if( (FindBookByName(library, book) != -1) || (library.getFirstEmpyIndex() == Const.storageCopacity))
       {
           return library.getStorage();
       }
       else
       {
           library.getStorage()[library.getFirstEmpyIndex()] = book;
           return library.getStorage();
       }

    }
    public Book[] DelBook(Library library,Book book) {

        if (FindBookByName(library, book) == -1)
        {
            return library.getStorage();
        }
        else
        {
            library.getStorage()[FindBookByName(library, book)] = null ;
            return library.getStorage();
        }
    }
    public int FindBookByName(Library library, Book book) {

        for (int i = 0; i<library.getFirstEmpyIndex();i++)
        {
            if (library.getStorage()[i].equals(book))
                return i;
        }
        return -1;

    }
}
