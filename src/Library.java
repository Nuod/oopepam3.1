import java.util.Arrays;

public class Library {
    private int firstEmpyIndex;
    private Book[] storage = new Book[Const.storageCopacity];

    public boolean isEmpty() {
        return firstEmpyIndex == 0;
    }

    public int getFirstEmpyIndex() {
        return firstEmpyIndex;
    }

    public Book[] getStorage() {
        return storage;
    }

    public void setStorage(Book[] storage) {
        firstEmpyIndex = updateFirstEmptyIndex();
        this.storage = storage;
    }
    private int updateFirstEmptyIndex() {
        if (storage[Const.storageCopacity-1] != null)
            return Const.storageCopacity;
        for(int i = Const.storageCopacity-1; i>=0 ;i--)
        {
            if(storage[i] != null)
                return i+1;
        }
        return 0;
    }
    @Override
    public String toString() {
        return "Library\n======================\n" +
                Arrays.toString(storage) +
                "\n======================\n ";
    }
}
